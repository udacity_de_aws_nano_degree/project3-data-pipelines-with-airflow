# Project: Data Pipelines with Airflow
## Introduction
A music streaming company, Sparkify, has decided that it is time to introduce more automation and monitoring to their data warehouse ETL pipelines and come to the conclusion that the best tool to achieve this is Apache Airflow.

They have decided to bring you into the project and expect you to create high grade data pipelines that are dynamic and built from reusable tasks, can be monitored, and allow easy backfills. They have also noted that the data quality plays a big part when analyses are executed on top the data warehouse and want to run tests against their datasets after the ETL steps have been executed to catch any discrepancies in the datasets.

The source data resides in S3 and needs to be processed in Sparkify's data warehouse in Amazon Redshift. The source datasets consist of JSON logs that tell about user activity in the application and JSON metadata about the songs the users listen to.

## Prequisites
Following the `project_prequisites.md`
## Data modeling

![data modeling](assets\data_modeling.JPG)

## Project instruction
Following the `project_instruction.md`
## Data ETL
Before run Airflow DAG
run `bash set_connections.sh` to setup connection variables for connect to AWS and RedshiftServerless

Open airflow UI and Active DAG to run
![airflow UI 1](assets\airflowUI1.JPG)

![dag active](assets\DAG_active.JPG)

Choosing dag active, and trigger DAG to run

![dag graph](assets\trigger_DAG.JPG)

## Data query

Access the IAM on AWS console with user and password. (In `.csv` file we download in `project_prequisites.md`)

![redshift serverless](assets\redshift_serverless.JPG)

Choosing `query data`

![data query](assets\data_query.JPG)

**Ex**: query data on `songplays` table
```
select * from songplays
```

![data query result](assets\data_query_rslt.JPG)


## Clearning 
For drop all table 
Before run Airflow DAG
run `bash set_connections.sh` to setup connection variables for connect to AWS and RedshiftServerless
Access Airflow and run DAG named: `drop_table`

![drop table dag](assets\dag_drop_table.JPG)

Run this dag will remove the tables on the redshift serverless





