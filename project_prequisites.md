# Project prequisites

### 1. [Install Airflow](https://airflow.apache.org/docs/apache-airflow/1.10.14/installation.html)

### 2. Create an IAM user in AWS
* Open `aws management console`
* Search `IAM Users` in the search bar, and then click on `Users`. 
* Click on `Add Users`
* Input `User name`.
* Check the field `Access key - Programmatic access`.
* Click on Next Permissions.

![add user](assets\access-key.png)

* In the `Set permissions` section, select `Attach existing policies` directly.
* Search and select the following policies:
    - AdministratorAccess
    - AmazonRedshiftFullAccess
    - AmazonS3FullAccess

* Click on `Next: Tags`, and the click on `Next Review`.

![user-permission](assets\user-permissions.png)

* You will see a Review section. Ensure that you have the same permissions as in the image below.
* Finally, click on `Create user`.

![user-review](assets\user-review.png)

* You should see a success message and user credentials on the next screen.
* Click on `Download .csv` to download the credentails for the user you just created. These credentials would be used in `connect Airflow to AWS`

#### Note: 
* For data query, you also need set IAM user can access with user, password.
* Go to IAM and click User that u has been created. 

![usr-psw_access](assets\usr_psw_access.JPG)

* Enable with console access with user, password

![usr-psw_access2](assets\usr-psw2.JPG)

![usr-psw_access3](assets\usr-psw3.JPG)

* Download `.csv` file. We will use this user and password for login this IAM user for Data query

### 3. Configure Redshift Serverless in AWS

* AWS Redshift Serverless gives you all the benefits of a Redshift cluster without paying for compute when your servers are idle. This is a tremendous cost savings. In fact, you should be able to stay within your Udacity credits.

* 3.1 Grant Redshift access to S3 so it can copy data from CSV files

Create a Redshift Role called `my-redshift-service-role` from the AWS Cloudshell:

```
aws iam create-role --role-name my-redshift-service-role --assume-role-policy-document '{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "redshift.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}'
```

* Now give the role S3 Full Access:
```
aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --role-name my-redshift-service-role
```

* 1. Open `AWS Cloud Console` button in the classroom.
* 2. Search `Redshift` in the search bar, and then click on `Amazon Redshift`.
* 3. Click `Redshift Serverless`

![amazon redshift](assets\amazon-redshift.png)

* 4. Click `Customize settings`

![redshift serverless customize setting](assets\screen-shot-2022-08-09-at-11.29.13-am.jpeg)

* 5. Go with the default namespace name

* 6. Check the box Customize admin user credentials

* 7. Enter `awsuser` for the Admin user name

* 8. Enter a password (save this for later)

![redshift serverless user name](assets\screen-shot-2022-08-10-at-12.58.39-pm.jpeg)

* 9. Associate the` my-redshift-service-role` you created with Redshift (Hint: If the role you created didn't show up, refresh the page)
* 10. This will enable Redshift Serverless to connect with S3

![redshift connect to s3](assets\screen-shot-2022-08-09-at-11.29.30-am.jpeg)

* 11. Accept the defaults for `Security and encryption`

![redshift default security and encryption](assets\screen-shot-2022-08-09-at-11.29.39-am.jpeg)

* 12. Accept the default `Workgroup` settings

![default workgroup setting](assets\screen-shot-2022-08-09-at-11.29.48-am.jpeg)

* 13. Select `Turn on enhanced VPC routing` and click `Save`

![turn on enhanced VPC routing](assets\screen-shot-2022-08-09-at-11.29.59-am.jpeg)

* 14. Click `Continue` and wait for `Redshift Serverless` setup to finish

![waiting redshift serverless](assets\screen-shot-2022-08-09-at-12.42.13-pm.jpeg)

* 15. On succesful completion, you will see Status Available, as shown below:

![available status](assets\screen-shot-2022-08-09-at-12.48.27-pm.jpeg)

* 16. Click the default Workgroup.

* 17. Next, we are going to make this cluster publicly accessible as we would like to connect to this cluster via Airflow.

* 18. Click Edit

![make cluster public accessible](assets\screen-shot-2022-08-09-at-12.49.12-pm.jpeg)

* 19. Select Turn on Publicly accessible

* 20. Click Save

![make cluster public access](assets\screen-shot-2022-08-09-at-12.50.00-pm.jpeg)

* 21. Modify public accessible

![modify public accessible](assets\public-accessibility.png)

* 22. Choose the link labeled `VPC security group` to open the Amazon Elastic Compute Cloud (Amazon EC2) console.

![VPC security group](assets\screen-shot-2022-08-09-at-12.52.42-pm.jpeg)

![VPC securiry group link](assets\screen-shot-2022-08-09-at-12.53.03-pm.jpeg)

![Open VPC security group](assets\vpc-sg.png)

* 23. Go to `Inbound Rules` tab and click on `Edit inbound rules`.

![Edit Inbound Rules](assets\screen-shot-2022-08-09-at-12.53.56-pm.jpeg)

* 24. Add an inbound rule, as shown in the image below.
        - Type = Custom TCP
        - Port range = 0 - 5500
        - Source = Anywhere-iPv4

![Add inbound rule](assets\screen-shot-2022-08-09-at-12.54.43-pm.jpeg)

* 25. Now Redshift Serverless should be accessible from Airflow.

* 26. Go back to the `Redshift Workgroup` and **copy** the `endpoint`. Store this locally as we will need this while configuring Airflow.

![Copy the redshift cluster endpoint](assets\screen-shot-2022-08-09-at-12.59.58-pm.jpeg)

### 4. Setting up connections
#### 4.1 Connect Airflow to AWS
Here, we'll use Airflow's UI to configure your AWS credentials.
1. To go to the Airflow UI:
* Once you see the message "Airflow web server is ready" click on the blue Access Airflow button in the bottom right.
2. Click on the `Admin` tab and select `Connections`.

![1. connect airflow to aws](assets\screen-shot-2022-08-09-at-10.06.13-am.jpeg)

3. Under `Connections`, click the plus button.

![2 add connect button](assets\screen-shot-2022-08-09-at-10.08.45-am.jpeg)

4. On the create connection page, enter the following values
* Connection Id: Enter `aws_credentials`.
* Connection Type: Enter `Amazon Web Services`.
* AWS Access Key ID: Enter your Access key ID from the IAM User credentials you downloaded earlier.
* AWS Secret Access Key: Enter your Secret access key from the IAM User credentials you downloaded earlier. Once you've entered these values, select Save.
* Click the `Test` button to pre-test your connection information.
* If not show error click `save`
**Note: The Access key ID and Secret access key should be taken from the csv file you downloaded after creating an IAM User on the page Create an IAM User in AWS.**
**HINT: Saving connection**

* After setting up your AWS Connection in Airflow, you need to save it in the following script in the workspace, so that it won't get lost after a refresh.
* In server that airflow installed, run the follwing command and observe the JSON output:
```
airflow connections get aws_credentials -o json 
```
* Assume that output is
```
[{"id": "1", 
 "conn_id": "aws_credentials",
 "conn_type": "aws", 
 "description": "", 
 "host": "", 
 "schema": "", 
 "login": "AKIA4QE4NTH3R7EBEANN", 
 "password": "s73eJIJRbnqRtll0/YKxyVYgrDWXfoRpJCDkcG2m", 
 "port": null, 
 "is_encrypted": "False", 
 "is_extra_encrypted": "False", 
 "extra_dejson": {}, 
 "get_uri": "aws://AKIA4QE4NTH3R7EBEANN:s73eJIJRbnqRtll0%2FYKxyVYgrDWXfoRpJCDkcG2m@"
}]
```
* Copy the value after "get_uri":
```
aws://AKIA4QE4NTH3R7EBEANN:s73eJIJRbnqRtll0%2FYKxyVYgrDWXfoRpJCDkcG2m@
```
* Update the following command with the URI and un-comment it, in `set_connections.sh` file
```
airflow connections add aws_credentials --conn-uri 'aws://AKIA4QE4NTH3R7EBEANN:s73eJIJRbnqRtll0%2FYKxyVYgrDWXfoRpJCDkcG2m@'
```
* When u start your airflow. Instead of your access airflow UI and execute `step 1 to step 4 in 4.1` for connect airflow to aws, you just need run `bash set_connections.sh` and DONE



#### 4.2 Connect Airflow to AWS Redshift serverless

Here, we'll use Airflow's UI to connect to AWS Redshift

* 1. Go to the Airflow UI

* 2. Click on the `Admin` tab and select `Connections`.

![Click on the Admin tab and select Connections.](assets\screen-shot-2022-08-09-at-10.06.13-am.jpeg)


* 3. Under `Connections`, select `Create`.

![Under Connections, click the plus button.](assets\screen-shot-2022-08-09-at-10.08.45-am.jpeg)

![In AWS Redshift Serverless Dashboard, click the default Workgroup](assets\screen-shot-2022-08-09-at-12.59.32-pm.jpeg)

![Copy the Redshift endpoint, found by clicking the Workgroup link](assets\screen-shot-2022-08-09-at-12.59.58-pm.jpeg)

* 4. On the Airflow create connection page, enter the following values:

    -   Connection Id: Enter `redshift`.
    -   Connection Type: Choose `Amazon Redshift`.
    -   Host: Enter the endpoint of your Redshift Serverless workgroup, excluding the port  and schema name at the end. You can find this by selecting your workgroup in the Amazon Redshift console. See where this is located in the screenshot below. 
    
        IMPORTANT: Make sure to **NOT** include the port and schema name at the end of the Redshift endpoint string.
    -   Schema: Enter `dev`. This is the Redshift database you want to connect to.
    -   Login: Enter `awsuser`.
    -   Password: Enter the password you created when launching Redshift serverless.
    -   Port: Enter `5439`. Once you've entered these values, select `Save`.

![airflow setup connect to redshift serverless](assets\screen-shot-2022-08-10-at-10.52.28-am.jpeg)

**HINT: Saving Connections**

After setting up your Redshift Connection in Airflow, you need to save it in the following script in the workspace, so that it won't get lost after a refresh.

On server that airflow installed, run the follwing command and observe the JSON output: 

```
airflow connections get redshift -o json
```

Ex: Assume output is

```
[{"id": "3", 
"conn_id": "redshift", 
"conn_type": "redshift", 
"description": "", 
"host": "default.859321506295.us-east-1.redshift-serverless.amazonaws.com", 
"schema": "dev", 
"login": "awsuser", 
"password": "R3dsh1ft", 
"port": "5439", 
"is_encrypted": "False", 
"is_extra_encrypted": "False", 
"extra_dejson": {}, 
"get_uri": "redshift://awsuser:R3dsh1ft@default.859321506295.us-east-1.redshift-serverless.amazonaws.com:5439/dev"}]
```

Copy the value after "get_uri":

```
redshift://awsuser:R3dsh1ft@default.859321506295.us-east-1.redshift-serverless.amazonaws.com:5439/dev
```

Modify `set_connections.sh`. Adding a line to this file

airflow connections add redshift --conn-uri 'redshift://awsuser:R3dsh1ft@default.859321506295.us-east-1.redshift-serverless.amazonaws.com:5439/dev'


After that. Instead of following the setup configure steps above for connect Airflow to redshift serverless. We just need run `bash set_connections.sh` and DONE


