# !/bin/bash
#
# Step 1: run the follwing command and observe the JSON output: 
# airflow connections get aws_credentials -o json 
# Output:  
# [{"id": "67", 
# "conn_id": "aws_credentials", 
# "conn_type": "aws", 
# "description": "", 
# "host": "", 
# "schema": "", 
# "login": "AKIAZE3SZRB2BOEXXXXX", 
# "password": "SkrSIK70BMZ35Sa1O/BqG9gY0NknW1d4LuKaxXNo", 
# "port": null, 
# "is_encrypted": "True", 
# "is_extra_encrypted": "True", 
# "extra_dejson": {}, 
# "get_uri": "aws://AKIAZE3SZRB2BOEXXXXX:SkrSIK70BMZ35Sa1O%2FBqG9gY0NknW1d4LuKaxXNo@"}]
#
# Step2: Copy the value after "get_uri":
#
# For example: aws://AKIAZE3SZRB2BOEXXXXX:SkrSIK70BMZ35Sa1O%2FBqG9gY0NknW1d4LuKaxXNo@
#
# Step3: Update the following command with the URI and un-comment it:
#
airflow connections add aws_credentials --conn-uri 'aws://AKIAZE3SZRB2BOEXXXXX:SkrSIK70BMZ35Sa1O%2FBqG9gY0NknW1d4LuKaxXNo@'
#
#
# Step6: run the follwing command and observe the JSON output: 
# airflow connections get redshift -o json
# 
# [{"id": "68", 
# "conn_id": "redshift", 
# "conn_type": "redshift", 
# "description": "", 
# "host": "default.628916586612.us-east-1.redshift-serverless.amazonaws.com", 
# "schema": "dev", 
# "login": "awsuser", 
# "password": "R3dsh1ft", 
# "port": "5439", 
# "is_encrypted": "True", 
# "is_extra_encrypted": "True", 
# "extra_dejson": {}, 
# "get_uri": "redshift://awsuser:R3dsh1ft@default.628916586612.us-east-1.redshift-serverless.amazonaws.com:5439/dev"}]
#
# Copy the value after "get_uri":
#
# For example: redshift://awsuser:R3dsh1ft@default.628916586612.us-east-1.redshift-serverless.amazonaws.com:5439/dev
#
# Step7: Update the following command with the URI and un-comment it:
#
airflow connections add redshift --conn-uri 'redshift://awsuser:R3dsh1ft@default.628916586612.us-east-1.redshift-serverless.amazonaws.com:5439/dev'
#
# Step4: update the following bucket name to match the name of your S3 bucket and un-comment it:
#
airflow variables set s3_bucket bitano-murdock
#
# Step5: un-comment the below line:
#
airflow variables set s3_prefix data-pipelines